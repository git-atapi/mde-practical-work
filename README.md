# Model Driven Engineering Labs

## Preparation

The exercises proposed here require the use of the Eclipse platform, which is available at its website: https://www.eclipse.org/downloads/

Eclipse comes in several flavors.  You can install either the Eclipse Modeling Tools (recommended) or the Eclipse IDE. You can also install the required plugins in your current installed version of Eclipse. 
The following plugins are required:

- EMF, the Eclipse Modeling Framework (v. 2.19), update site: http://download.eclipse.org/modeling/emf/emf/builds/release/2.19
- ATL, the Atlanmod Transformation Language (v. 4.1.0), update site: http://download.eclipse.org/mmt/atl/updates/releases

To install Eclipse plugins from update site, you must point your Install Manager at these sites.
Go to `Help > Software Updates... > Available Software > Add Site...` and add the concerned update site.

## Laboratories

1. [Metamodeling](./meta-modeling/)
2. [EMF - Eclipse Modeling Framework](./emf-tutorial/)
3. Create a simple ATL transformation [Families to Persons](./families-persons/)
4. ATL Transformation [Binder to Summary](./binder-summary/)
5. [Modisco](./modisco/)
