package tp.fil.main;

import java.io.IOException;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

public class MetricsComputation {
	
	public static void main(String[] args) {
		try {
			Resource javaMetamodel;
			Resource javaModel;
			Resource metricsModel;
			Resource metricsMetamodel;
			
			//Create and configure resource set
			ResourceSet resSet = new ResourceSetImpl();
			resSet.getResourceFactoryRegistry().
				getExtensionToFactoryMap().
				put("ecore", new EcoreResourceFactoryImpl());
			XMIResourceFactoryImpl xmiFactory = new XMIResourceFactoryImpl();
			resSet.getResourceFactoryRegistry().
				getExtensionToFactoryMap().
				put("xmi", xmiFactory);
			
			//Load Java & Metrics metamodel
			javaMetamodel = resSet.createResource(URI.createFileURI("src/tp/fil/resources/Java.ecore"));
			javaMetamodel.load(null);
			EPackage.Registry.INSTANCE.put("http://www.eclipse.org/MoDisco/Java/0.2.incubation/java", 
					javaMetamodel.getContents().get(0));
			
			metricsMetamodel = resSet.createResource(URI.createFileURI("src/tp/fil/resources/Metrics.ecore"));
			metricsMetamodel.load(null);
			EPackage.Registry.INSTANCE.put("http://metrics", 
					metricsMetamodel.getContents().get(0));
			
			//Load Java model
			javaModel = resSet.createResource(URI.createFileURI("../PetStore/PetStore_java.xmi"));
			javaModel.load(null);
			
			//Initiate Metrics model
			metricsModel = resSet.createResource(URI.createFileURI("../PetStore/PetStore_metrics.xmi"));
			
			/*
			 * TO BE COMPLETED...
			 */
			
			//Serialize Metrics model
			metricsModel.save(null);
			
			//Unload models and metamodels
			javaModel.unload();
			metricsModel.unload();
//			EPackage.Registry.INSTANCE.remove("http://www.eclipse.org/MoDisco/Java/0.2.incubation/java");
//			javaMetamodel.unload();
//			EPackage.Registry.INSTANCE.remove("http://metrics");
//			metricsMetamodel.unload();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
