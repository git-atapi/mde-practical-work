<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm version="1.0" name="0">
	<cp>
		<constant value="MetricsComputation"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="Map"/>
		<constant value="#native"/>
		<constant value="1"/>
		<constant value="Sequence"/>
		<constant value="Package"/>
		<constant value="JAVA"/>
		<constant value="J.allInstances():J"/>
		<constant value="J.asSequence():J"/>
		<constant value="2"/>
		<constant value="name"/>
		<constant value="petstore"/>
		<constant value="J.=(J):J"/>
		<constant value="B.not():B"/>
		<constant value="22"/>
		<constant value="CJ.including(J):CJ"/>
		<constant value="J.first():J"/>
		<constant value="ownedPackages"/>
		<constant value="ownedElements"/>
		<constant value="3"/>
		<constant value="ClassDeclaration"/>
		<constant value="J.oclIsTypeOf(J):J"/>
		<constant value="46"/>
		<constant value="J.size():J"/>
		<constant value="J.including(JJ):J"/>
		<constant value="J.toString():J"/>
		<constant value="../PetStore/PetStore_metrics.txt"/>
		<constant value="J.writeTo(J):J"/>
		<constant value="5:46-5:52"/>
		<constant value="2:2-2:14"/>
		<constant value="2:2-2:29"/>
		<constant value="2:2-2:43"/>
		<constant value="3:15-3:16"/>
		<constant value="3:15-3:21"/>
		<constant value="3:24-3:34"/>
		<constant value="3:15-3:34"/>
		<constant value="2:2-3:35"/>
		<constant value="2:2-4:11"/>
		<constant value="2:2-4:25"/>
		<constant value="6:3-6:8"/>
		<constant value="7:4-7:5"/>
		<constant value="7:4-7:10"/>
		<constant value="8:4-8:5"/>
		<constant value="8:4-8:19"/>
		<constant value="8:32-8:33"/>
		<constant value="8:46-8:67"/>
		<constant value="8:32-8:68"/>
		<constant value="8:4-8:69"/>
		<constant value="8:4-8:77"/>
		<constant value="6:3-8:78"/>
		<constant value="2:2-9:3"/>
		<constant value="2:2-10:13"/>
		<constant value="10:22-10:56"/>
		<constant value="2:2-10:57"/>
		<constant value="p"/>
		<constant value="myMap"/>
		<constant value="self"/>
	</cp>
	<operation name="1">
		<context type="2"/>
		<parameters>
		</parameters>
		<code>
			<push arg="3"/>
			<push arg="4"/>
			<new/>
			<store arg="5"/>
			<push arg="6"/>
			<push arg="4"/>
			<new/>
			<push arg="7"/>
			<push arg="8"/>
			<findme/>
			<call arg="9"/>
			<call arg="10"/>
			<iterate/>
			<store arg="11"/>
			<load arg="11"/>
			<get arg="12"/>
			<push arg="13"/>
			<call arg="14"/>
			<call arg="15"/>
			<if arg="16"/>
			<load arg="11"/>
			<call arg="17"/>
			<enditerate/>
			<call arg="18"/>
			<get arg="19"/>
			<iterate/>
			<store arg="11"/>
			<load arg="5"/>
			<load arg="11"/>
			<get arg="12"/>
			<push arg="6"/>
			<push arg="4"/>
			<new/>
			<load arg="11"/>
			<get arg="20"/>
			<iterate/>
			<store arg="21"/>
			<load arg="21"/>
			<push arg="22"/>
			<push arg="8"/>
			<findme/>
			<call arg="23"/>
			<call arg="15"/>
			<if arg="24"/>
			<load arg="21"/>
			<call arg="17"/>
			<enditerate/>
			<call arg="25"/>
			<call arg="26"/>
			<store arg="5"/>
			<enditerate/>
			<load arg="5"/>
			<call arg="27"/>
			<push arg="28"/>
			<call arg="29"/>
		</code>
		<linenumbertable>
			<lne id="30" begin="0" end="2"/>
			<lne id="31" begin="7" end="9"/>
			<lne id="32" begin="7" end="10"/>
			<lne id="33" begin="7" end="11"/>
			<lne id="34" begin="14" end="14"/>
			<lne id="35" begin="14" end="15"/>
			<lne id="36" begin="16" end="16"/>
			<lne id="37" begin="14" end="17"/>
			<lne id="38" begin="4" end="22"/>
			<lne id="39" begin="4" end="23"/>
			<lne id="40" begin="4" end="24"/>
			<lne id="41" begin="27" end="27"/>
			<lne id="42" begin="28" end="28"/>
			<lne id="43" begin="28" end="29"/>
			<lne id="44" begin="33" end="33"/>
			<lne id="45" begin="33" end="34"/>
			<lne id="46" begin="37" end="37"/>
			<lne id="47" begin="38" end="40"/>
			<lne id="48" begin="37" end="41"/>
			<lne id="49" begin="30" end="46"/>
			<lne id="50" begin="30" end="47"/>
			<lne id="51" begin="27" end="48"/>
			<lne id="52" begin="0" end="51"/>
			<lne id="53" begin="0" end="52"/>
			<lne id="54" begin="53" end="53"/>
			<lne id="55" begin="0" end="54"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="56" begin="13" end="21"/>
			<lve slot="3" name="56" begin="36" end="45"/>
			<lve slot="2" name="56" begin="26" end="49"/>
			<lve slot="1" name="57" begin="3" end="51"/>
			<lve slot="0" name="58" begin="0" end="54"/>
		</localvariabletable>
	</operation>
</asm>
