# ATL Transformation: Binder2Summary



The objective of this exercise is to write an ATL transformation capable of summarizing the content of a workbook.
More precisely, to transform instances of the `Binder` model into instances of the `Summary` model, both provided in the `./src/model/` folder.


### Input and Output models

The `Binder` modela contains two classes, `Binder` et `Chapter`:

```
	class Binder
	{
		attribute title : String[1];
		property chapters#book : Chapter[*|1] { ordered composes };
	}
	class Chapter
	{
		attribute title : String[1];
		attribute nbPages : ecore::EInt[1];
		attribute author : String[1];
		property book#chapters : Binder[1];
	}
```
The `Summary` model cotains only one class, `Article`:

```
	class Article
	{
		attribute title : String[1];
		attribute authors : String[1];
		attribute nbPages : ecore::EInt[1];
	}
```

###  ATL Transformation

The transformation you need to write should create an `Article` instance for each `Binder` instance.
The title of the article will be the same as the title of the `Binder`, while the attribute `outside` will receive the authors' names of the chapters in turn and the attribute `nbPages` will receive the sum of all the pages of the chapters.


Suppose you have the following `Binder` model instance as an input:

```
Binder title="Quantum Physics" chapters={
    Chapter title="Cat" nbPages=10 author="Schrödinger";
    Chapter title="The Quanta" nbPages=11 author="Planck";
}
```

Youshould get the following `Summary` model instance output:

```
Article title="Quantum Physics" authors="Schrödinger and Planck" nbPages=21;
```

### Practical advices

- An example containing 2 instances of `Binder` and several instances of `Chapter` is provided in the `./input` folder. Use it as a test data for your transformation.

- Take a look at the syntax of the operation [**Iterate**](https://sunye.github.io/ocl/#/0/84), it will be used a lot to build the attribute `other than`Article` class`.




