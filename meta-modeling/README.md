#  Metamodeling Lab

## Goal

The goal of this lab is to introduce the base concepts of Meta-modeling, which in software engineering means the
use a modeling language to model the abstract syntax of another modeling language. 
For instance, using the UML to model the abstract syntax of Petri Nets.

## The Entity-Relationship Modeling Language

The Entity-Relationship (ER) modeling language was introduced by Peter Chen 1976 for representing abstract data models that specify the information structure that will be stored in a database system.

The main modeling concepts of ER are: entity types, relationship types, attributes, roles, and cardinalities.
More precisions:
    
- All three main concepts are identified by a name.
- Both, entities and relationships may have attributes.
- Relationships link two or more entities. 
- Relationships and Entities are linked through *Roles*.
- A role has a *name* and a *cardinality*.
- An attribute may or not be a primary key.
- Entities are either *weak* or *strong*.

![An entity–relationship diagram for an MMORPG using Chen's notation (from Wikipedia).](https://upload.wikimedia.org/wikipedia/commons/7/72/ER_Diagram_MMORPG.png)*An entity–relationship diagram for an MMORPG using Chen's notation (from Wikipedia).*

## Exercise 1 - Use the Ecore to model the abstract syntax of ER.

The abstract syntax of a language defines its concepts and their relationships. It specifies, for instance, the the ER has entities and relationships, that and relationships link two or more entities, and that entities may be linked to 0 or more relationships.

In this first exercise, you will propose a meta-model for ER, using the Ecore. Proceed as follows:

1. Open the `entity-relationship` file, which is inside `model > meta-models.aird > Design > Entities in a Class Diagram`.
1. Identify all modeling concepts that compose the language.
2. Represent these concepts as Ecore classes. Do not worry about inheritance/sub-typing at first.
3. Use Ecore references to represent how the concepts relate to each other. For instance, that entities and relationships may have attributes.

## Exercise 2 - Use the Ecore to model the abstract syntax of the Relational Model

The relational model has two main concepts: *Table* and *Column*. More precisions:

- A table has one or more columns and a column belongs to only one table.
- Tables and Columns have names.
- Columns have types.
- Tables have one or more columns that are primary keys.
- Tables may have columns that are foreign keys (primary keys from other tables).

Use the Ecore to represent the abstract syntax of this modeling language.

## Exercise 3 - Use the Ecore to model the abstract syntax of Petri Nets

Petri net are directed bipartite graphs, where nodes represent transitions (i.e., events that may occur, represented by bars) and places (i.e., conditions, represented by circles). The directed arcs describe which places are pre- and/or postconditions for which transitions (signified by arrows).

![](https://upload.wikimedia.org/wikipedia/commons/d/d7/Animated_Petri_net_commons.gif)*Petri net trajectory example (from Wikipedia).*

1. Open the `petri-nets` file, which is inside `model > meta-models.aird > Design > Entities in a Class Diagram`.
1. Design a meta-model for Petri Nets.

## Exercise 4 - Use the Ecore to model the abstract syntax of  UML Classes and Attributes

Use the Ecore to represent the abstract syntax of UML. 
Start with two concepts: *Class* and *Attribute*. More precisions:

- A class may have any number of attributes.
- An attribute has a name, a type, a visibility, and a cardinality.
- An attribute may have properties: readOnly, identifier, derived.
- An attribute has a level: instance or a class.

## Exercise 5 - Use the Ecore to model the abstract syntax of  UML Operations

Improve the previous diagram to integrate the concept of *Operation*:

- A class may have any number of operations.
- An operation has a name, a return type, a visibility and an ordered list of parameters.
- Each parameter has a name, a type, and a direction: in, out, or inout.
